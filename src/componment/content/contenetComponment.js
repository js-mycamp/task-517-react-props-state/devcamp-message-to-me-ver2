import { Component } from "react";
import Input from './input/inputContent'
import Output from './output/outputContent'

class Content extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputMessage:"",
            outputMessage:[],
            likeDisplay: false,
        }
    }

    inputMessageChangeHandler = (value) =>{
        this.setState({
            inputMessage: value
        })
    } 
    outputMessageChangeHandler = () => {
        if(this.state.inputMessage){
            this.setState({
                outputMessage : [...this.state.outputMessage, this.state.inputMessage],
                // outputMessage: this.state.inputMessage,
                likeDisplay : true,
            })
        }
    }
    render() {
        return (
            <>
                <Input inputMessageProps={this.state.inputMessage} inputMessageChangeHandlerProps={this.inputMessageChangeHandler} outputMessageChangeHandlerProps={this.outputMessageChangeHandler}/>
                <Output outputMessageProp={this.state.outputMessage} likeDisplayProps={this.state.likeDisplay} />
            </>
    
        )
    }
}
export default Content