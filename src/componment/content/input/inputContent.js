import { Component } from "react";

class inputContent extends Component {
    onInputChangeHandler = (event) => {
        let value = event.target.value;
        console.log(value);

        this.props.inputMessageChangeHandlerProps(value);
    }

    onButtonClickHandler = () => {
        console.log("đã nhấn nút gởii!")
        this.props.outputMessageChangeHandlerProps();
    }

  
    render() {
        return (
            <>
                <div className='col-12 mt-2'>
                    <label className='form-floating'>Message cho bạn trong 12 tháng tới</label>
                </div>

                <div className='col-12 mt-2'>
                    <input className='form-control' onChange={this.onInputChangeHandler} placeholder="nhập message" style={{ width: "500px", margin: "0 auto" }}></input>
                </div>

                <div className=" row mt-2">
                    <div className='col-12 mt-2'>
                        <button className='btn btn-primary' onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                    </div>
                </div>

            </>
        )
    }
}

export default inputContent