import { Component } from "react";
import TextHeader from './textHeader/textHeader'
import ImageHeader from "./ImageHeader/imageHeader";
class Header extends Component{
    render(){
        return(
            <>
            <TextHeader />
            <ImageHeader />
            </>
        )
    }
}
export default Header